package com.okihita.inahrs.profile;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;
import com.okihita.inahrs.util.Config;
import com.okihita.inahrs.util.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity {

    private static final String QUESTION_MARK = "?";
    private final String TAG = Config.TAG;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    // LOGIN
    @BindView(R.id.profile_LL_loginForm)
    LinearLayout mLoginFormLL;
    @BindView(R.id.profile_ET_email)
    EditText mEmailEditText;
    // LOGGED-IN USER DETAIL
    @BindView(R.id.profile_LL_profileDetail)
    LinearLayout mProfileDetailLL;
    @BindView(R.id.profile_TV_name)
    TextView mNameTextView;
    @BindView(R.id.profile_TV_email)
    TextView mEmailTextView;
    @BindView(R.id.profile_TV_specialization)
    TextView mSpecializationTextView;
    @BindView(R.id.profile_TV_workshopsAttended)
    TextView mWorkshopsAttendedTextView;
    // Not a bug. This is according to the database used that GP starts from 5
    String[] specializations = {"null", "null", "null", "null", "null", "GP", "Cardiologist", "Other Specialists", "Nurse"};

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @OnClick(R.id.profile_Button_login)
    void requestLogin() {

        final String email = mEmailEditText.getText().toString();
        String loginUrl = Config.LOGIN_URL + QUESTION_MARK + "email=" + email;

        Log.d(TAG, "requestLogin: Login URL = " + loginUrl);
        JsonObjectRequest loginRequest = new JsonObjectRequest(
                loginUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d(TAG, "onResponse: " + response.toString());
                            fillSharedPreference(response);
                            Toast.makeText(ProfileActivity.this, "You're logged in!", Toast.LENGTH_SHORT).show();
                            finish();
                        } catch (JSONException e) {
                            Log.d(TAG, "onResponse: " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "onErrorResponse: " + error.getMessage());
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(loginRequest);
    }

    @OnClick(R.id.profile_Button_logout)
    void requestLogout() {
        SharedPreferences.Editor editor = PreferenceManager
                .getDefaultSharedPreferences(this.getApplicationContext())
                .edit();

        editor
                .putInt(getString(R.string.pref_symposia_specialization), 0)
                .apply();
        editor
                .putString(getString(R.string.pref_fullname), null)
                .apply();

        mProfileDetailLL.setVisibility(View.GONE);
        mLoginFormLL.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        setupToolbar();
        loadBannerImage();

        if (isLoggedIn()) showUserProfile();
        else showLoginForm();
    }

    private void showLoginForm() {
        mProfileDetailLL.setVisibility(View.GONE);
        mLoginFormLL.setVisibility(View.VISIBLE);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow_layer_list);
        getSupportActionBar().setTitle(null);
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder)
                .into(mBannerImageView);
    }

    private boolean isLoggedIn() {
        String username = PreferenceManager.getDefaultSharedPreferences(
                this.getApplicationContext()).getString(getString(R.string.pref_fullname), null);
        return username != null;
    }

    private void fillSharedPreference(JSONObject response) throws JSONException {
        SharedPreferences.Editor editor = PreferenceManager
                .getDefaultSharedPreferences(this.getApplicationContext())
                .edit();
        editor
                .putInt(getString(R.string.pref_participant_id), response.getJSONObject("profile").getInt("participant_id"))
                .apply();
        editor
                .putString(getString(R.string.pref_fullname), response.getJSONObject("profile").getString("fullname"))
                .apply();
        editor
                .putString(getString(R.string.pref_email), mEmailEditText.getText().toString())
                .apply();
        editor
                .putString(getString(R.string.pref_registrant_id), response.getJSONObject("profile").getString("registrant_id"))
                .apply();
        editor
                .putInt(getString(R.string.pref_symposia_specialization), response.getJSONArray("symposiumSpecialization").getInt(0))
                .apply();

        // Saving workshop's values
        editor
                .putInt(getString(R.string.pref_workshop_array_size), response.getJSONArray("workshopIds").length())
                .apply();

        for (int wsId = 0; wsId < response.getJSONArray("workshopIds").length(); wsId++) {
            String keyString = getString(R.string.pref_workshop_array_id_) + wsId;
            editor.putInt(keyString, response.getJSONArray("workshopIds").getInt(wsId));
            editor.apply();
        }
    }

    private void showUserProfile() {

        mLoginFormLL.setVisibility(View.GONE);
        mProfileDetailLL.setVisibility(View.VISIBLE);

        int symSpecId = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext())
                .getInt(getString(R.string.pref_symposia_specialization), 0);
        String symSpecName = specializations[symSpecId];
        mSpecializationTextView.setText(symSpecName);

        String fullname = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext())
                .getString(getString(R.string.pref_fullname), "user");
        mNameTextView.setText(fullname);

        String email = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext())
                .getString(getString(R.string.pref_email), "email");
        mEmailTextView.setText(email);

        String workshopIds = "Attending: ";
        int workshopSize = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext())
                .getInt(getString(R.string.pref_workshop_array_size), 0);

        for (int i = 0; i < workshopSize; i++) {
            workshopIds = workshopIds + String.valueOf(PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext())
                    .getInt(getString(R.string.pref_workshop_array_id_) + i, 0)) + ", ";
        }
        mWorkshopsAttendedTextView.setText(workshopIds);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
