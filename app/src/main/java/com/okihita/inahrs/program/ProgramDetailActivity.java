package com.okihita.inahrs.program;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;
import com.okihita.inahrs.util.Config;
import com.okihita.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProgramDetailActivity extends AppCompatActivity {

    public static final String PROGRAM_DETAIL_INDEX = "com.example.okihita.inahrs.program_detail";
    private static final String TAG = Config.TAG;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.programDetail_TV_programName)
    TextView mProgramNameTextView;
    @BindView(R.id.programDetail_IV_programImage)
    ImageView mProgramImageView;
    @BindView(R.id.programDetail_TV_programDescription)
    TextView mDescriptionTextView;
    @BindView(R.id.programDetail_TV_programWebsite)
    TextView mWebsiteTextView;
    @BindView(R.id.programDetail_TV_programPhone)
    TextView mPhoneTextView;
    @BindView(R.id.programDetail_TV_programEmail)
    TextView mEmailTextView;
    private int mProgramId;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_detail);
        ButterKnife.bind(this);

        mProgramId = getIntent().getIntExtra(PROGRAM_DETAIL_INDEX, 0);

        setupProgressDialog();
        setupToolbar();
        loadBannerImage();
        fetchProgramDetail();
    }

    ProgressDialog mProgressDialog;

    private void setupProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
    }

    private void fetchProgramDetail() {
        mProgressDialog.show();
        JsonArrayRequest programDetailRequest = new JsonArrayRequest(
                Config.PROGRAM_ENDPOINT + String.valueOf(mProgramId),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.d(TAG, "onResponse: " + response.toString());
                            JSONObject object = response.getJSONObject(0);
                            updateView(object);
                            mProgressDialog.dismiss();
                        } catch (JSONException e) {
                            Log.e(TAG, "onResponse: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "onErrorResponse: " + error.getMessage());
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(programDetailRequest);
    }

    private void updateView(JSONObject object) throws JSONException {
        Log.d(TAG, "updateView: ");
        mProgramNameTextView.setText(object.getString("name"));
        Glide.with(this).load(Config.ENDPOINT + object.getString("image")).into(mProgramImageView);
        mDescriptionTextView.setText(object.getString("description"));
        mWebsiteTextView.setText(object.getString("website"));
        mPhoneTextView.setText(object.getString("phone"));
        mEmailTextView.setText(object.getString("email"));
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder)
                .into(mBannerImageView);
    }

    @OnClick(R.id.programDetail_TV_programPhone)
    void callNumber() {

        String mPhoneNumber = mPhoneTextView.getText().toString();
        String uri = "tel:" + mPhoneNumber.trim();
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }

    @OnClick(R.id.programDetail_TV_programEmail)
    void sendEmail() {

        String emailAddress = mEmailTextView.getText().toString();
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", emailAddress, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "From InaHRS 2016");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    @OnClick(R.id.programDetail_TV_programWebsite)
    void gotoWebsite() {
        String websiteAddress = mWebsiteTextView.getText().toString();
        Uri uriUrl = Uri.parse(websiteAddress);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
