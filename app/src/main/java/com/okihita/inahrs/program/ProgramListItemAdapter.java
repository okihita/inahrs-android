package com.okihita.inahrs.program;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgramListItemAdapter extends RecyclerView.Adapter<ProgramListItemAdapter.ViewHolder> {

    List<ProgramListItem> mProgramListItems;

    public ProgramListItemAdapter(List<ProgramListItem> programListItems) {
        mProgramListItems = programListItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_program_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProgramListItem programListItem = mProgramListItems.get(position);

        Glide.with(holder.mProgramImageView.getContext()).load(programListItem.getImageUrl())
                .centerCrop()
                .into(holder.mProgramImageView);

        holder.mNameTextView.setText(programListItem.getName());
    }

    @Override
    public int getItemCount() {
        return mProgramListItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.programList_IV_image)
        ImageView mProgramImageView;
        @BindView(R.id.programList_TV_name)
        TextView mNameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewProgramDetail(view);
                }

                private void viewProgramDetail(View view) {
                    Context c = view.getContext();
                    Intent intent = new Intent(c, ProgramDetailActivity.class);
                    intent.putExtra(ProgramDetailActivity.PROGRAM_DETAIL_INDEX, mProgramListItems.get(getLayoutPosition()).getId());
                    c.startActivity(intent);
                }
            });
        }
    }
}
