package com.okihita.inahrs.program;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.okihita.inahrs.R;
import com.okihita.inahrs.util.Config;
import com.okihita.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProgramListActivity extends AppCompatActivity {

    private final String TAG = Config.TAG;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.program_RV_program)
    RecyclerView mProgramRecyclerView;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    List<ProgramListItem> mProgramListItems;
    RecyclerView.LayoutManager mLayoutManager;
    ProgramListItemAdapter mProgramListItemAdapter;
    ProgressDialog mProgressDialog;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_list);
        ButterKnife.bind(this);

        setupProgressDialog();
        setupToolbar();
        fetchProgramList();
        setupRecyclerView();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void setupProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
    }

    private void fetchProgramList() {
        mProgressDialog.show();
        mProgramListItems = new ArrayList<>();
        JsonArrayRequest hotelListRequest = new JsonArrayRequest(
                Config.PROGRAM_GET_LIST,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject object = response.getJSONObject(i);
                                mProgramListItems.add(new ProgramListItem(
                                        object.getInt("id"),
                                        object.getString("name"),
                                        Config.ENDPOINT + object.getString("image")
                                ));
                            }

                            mProgramListItemAdapter.notifyDataSetChanged();
                            mProgressDialog.dismiss();
                        } catch (JSONException e) {
                            Log.d(TAG, "onResponse: Error " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(hotelListRequest);
    }

    private void setupRecyclerView() {
        mLayoutManager = new LinearLayoutManager(this);
        mProgramRecyclerView.setLayoutManager(mLayoutManager);

        mProgramListItemAdapter = new ProgramListItemAdapter(mProgramListItems);
        mProgramRecyclerView.setAdapter(mProgramListItemAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

}
