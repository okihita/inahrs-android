package com.okihita.inahrs.program;


public class ProgramListItem {

    private int mId;
    private String mName;
    private String mImageUrl;

    public ProgramListItem(int id, String name, String imageUrl) {
        mId = id;
        mName = name;
        mImageUrl = imageUrl;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getImageUrl() {
        return mImageUrl;
    }
}
