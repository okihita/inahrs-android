package com.okihita.inahrs.util;

public class Config {


    //    public static final String ENDPOINT = "http://192.168.0.11/inahrs-mobile/"; // CyLo
//    public static final String ENDPOINT = "http://192.168.1.163/inahrs-mobile/"; // Bolt
//    public static final String ENDPOINT = "http://192.168.43.164/inahrs-mobile/"; // Hape sendiri
    public static final String ENDPOINT = "http://inahrs-mobile.inahrs.or.id/"; // InaHRS.or.id

    public static final String LOGIN_URL = ENDPOINT + "mobile-login";
    public static final String NOTIF_URL = ENDPOINT + "notif/all";

    public static final String SYMPOSIUM_ENDPOINT = ENDPOINT + "symposium/";
    public static final String SYMPOSIUM_GET_LIST_FOR_SPEC = SYMPOSIUM_ENDPOINT + "getAllForSpec/";
    public static final String SYMPOSIUM_GET_LIST = SYMPOSIUM_ENDPOINT + "all";

    public static final String WORKSHOP_ENDPOINT = ENDPOINT + "workshop/";
    public static final String WORKSHOP_GET_LIST = WORKSHOP_ENDPOINT + "all";

    public static final String QUESTION_ENDPOINT = ENDPOINT + "question/";
    public static final String Q_WORKSHOP_ENDPOINT = QUESTION_ENDPOINT + "workshop/";
    public static final String Q_SYMPOSIUM_ENDPOINT = QUESTION_ENDPOINT + "symposium/";

    public static final String PROGRAM_ENDPOINT = ENDPOINT + "program/";
    public static final String PROGRAM_GET_LIST = PROGRAM_ENDPOINT + "all";

    public static final String SPEAKER_ENDPOINT = ENDPOINT + "speaker/";
    public static final String SPEAKER_GET_LIST = SPEAKER_ENDPOINT + "all";

    public static final String HOTEL_ENDPOINT = ENDPOINT + "hotel/";
    public static final String HOTEL_GET_LIST = HOTEL_ENDPOINT + "all";

    public static final String SPONSOR_ENDPOINT = ENDPOINT + "sponsor/";
    public static final String SPONSOR_GET_LIST = SPONSOR_ENDPOINT + "all";

    public static final String TAG = "great25";


    public static final String TINYDB_SYMPO = "tinyDB_list_sympo";
    public static final String TINYDB_WORKSHOP = "tinyDB_list_workshop";

    public static final int NOTIFICATION_ID = 12;
}