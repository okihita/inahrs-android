package com.okihita.inahrs.sponsor;

import android.util.Log;

import com.okihita.inahrs.util.Config;

import org.json.JSONException;
import org.json.JSONObject;

public class Sponsor {

    private final String ID = "sponsor_id";
    private final String NAME = "sponsor_name";
    private final String IMAGE = "sponsor_image";
    private final String DESCRIPTION = "sponsor_description";
    private final String WEBSITE = "sponsor_website";
    private final String ADDRESS = "sponsor_address";
    private final String PHONE = "sponsor_phone";
    private final String EMAIL = "sponsor_email";
    private final String TYPE = "sponsor_type";

    private int mId;
    private String mName;
    private String mImageUrl;
    private String mDescription;
    private String mWebsite;
    private String mAddress;
    private String mPhone;
    private String mEmail;
    private String mType;

    public Sponsor(JSONObject json) throws JSONException {
        if (!json.isNull(ID)) mId = json.getInt(ID);
        if (!json.isNull(NAME)) mName = json.getString(NAME);
        if (!json.isNull(IMAGE)) mImageUrl = Config.ENDPOINT + json.getString(IMAGE);
        if (!json.isNull(DESCRIPTION)) mDescription = json.getString(DESCRIPTION);
        if (!json.isNull(WEBSITE)) mWebsite = json.getString(WEBSITE);
        if (!json.isNull(ADDRESS)) mAddress = json.getString(ADDRESS);
        if (!json.isNull(PHONE)) mPhone = json.getString(PHONE);
        if (!json.isNull(EMAIL)) mEmail = json.getString(EMAIL);
        if (!json.isNull(TYPE)) mType = json.getString(TYPE);
        Log.d("###", "Sponsor: " + mImageUrl);
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getWebsite() {
        return mWebsite;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getType() {
        return mType;
    }
}
