package com.okihita.inahrs.sponsor;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SponsorListItemAdapter extends RecyclerView.Adapter<SponsorListItemAdapter.ViewHolder> {

    private final int VIEW_TYPE_GOLD = 1;
    private final int VIEW_TYPE_SILVER = 2;
    private final int VIEW_TYPE_BRONZE = 3;

    List<Sponsor> mSponsors;

    public SponsorListItemAdapter(List<Sponsor> sponsors) {
        mSponsors = sponsors;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_GOLD:
                return new GoldViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gold_sponsor_list, parent, false));
            case VIEW_TYPE_SILVER:
                return new SilverBronzeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gold_sponsor_list, parent, false));
            case VIEW_TYPE_BRONZE:
                return new SilverBronzeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bronze_sponsor_list, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Sponsor sponsor = mSponsors.get(position);
        Glide.with(holder.mSponsorImageView.getContext()).load(sponsor.getImageUrl())
                .into(holder.mSponsorImageView);
    }

    @Override
    public int getItemViewType(int position) {
        Sponsor sponsor = mSponsors.get(position);
        switch (sponsor.getType()) {
            case "gold":
                Log.d("###", "getItemViewType: Gold!");
                return VIEW_TYPE_GOLD;
            case "silver":
                Log.d("###", "getItemViewType: Silver");
                return VIEW_TYPE_SILVER;
            case "bronze":
                Log.d("###", "getItemViewType: Bronze");
                return VIEW_TYPE_BRONZE;
            default:
                return 0;
        }
    }

    @Override
    public int getItemCount() {
        return mSponsors.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.sponsorList_IV_image)
        ImageView mSponsorImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * If the sponsor type is silver or bronze, when it's clicked go to its website.
     */
    public class SilverBronzeViewHolder extends ViewHolder {
        public SilverBronzeViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context c = view.getContext();
                    Sponsor item = mSponsors.get(getLayoutPosition());

                    Uri uriUrl = Uri.parse(item.getWebsite());
                    Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                    c.startActivity(launchBrowser);
                }
            });
        }
    }

    public class GoldViewHolder extends ViewHolder {
        public GoldViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context c = view.getContext();
                    Sponsor item = mSponsors.get(getLayoutPosition());

                    Intent intent = new Intent(c, SponsorDetailActivity.class);
                    intent.putExtra(SponsorDetailActivity.SPONSOR_ID, item.getId());
                    c.startActivity(intent);

                }
            });
        }
    }
}
