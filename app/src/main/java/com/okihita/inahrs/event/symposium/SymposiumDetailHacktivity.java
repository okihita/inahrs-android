package com.okihita.inahrs.event.symposium;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;
import com.okihita.inahrs.util.Config;
import com.okihita.inahrs.util.TinyDB;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SymposiumDetailHacktivity extends AppCompatActivity {

    public static final String EVENT_DETAIL_INDEX = "com.example.okihita.inahrs.event_detail";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;

    @BindView(R.id.titleImage)
    ImageView mTitleImage;
    @BindView(R.id.workshopBookmark)
    ImageView mWorkshopBookmark;
    @BindView(R.id.workshopContent)
    ImageView mWorkshopContent;

    TinyDB mTinyDB;
    ArrayList<Integer> mBookmarkedSymposiumIds = new ArrayList<>();
    private int mSymposiumId;
    private boolean isBookmarked = false;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @OnClick(R.id.askQuestionButton)
    void gotoAskQuestion() {
        Intent intent = new Intent(this, AskSympoQuestionActivity.class);
        intent.putExtra(AskSympoQuestionActivity.SYMPOSIUM_ID, mSymposiumId);
        startActivity(intent);
    }

    @OnClick(R.id.workshopBookmark)
    void onBookmarkClicked() {
        Log.d("###", "onBookmarkClicked: Clicked!");
        if (!isBookmarked) addBookmark();
        else removeBookmark();
    }

    private void addBookmark() {
        Log.d("###", "addBookmark: Adding bookmark.");
        mWorkshopBookmark.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_big_active));
        mBookmarkedSymposiumIds.add(mSymposiumId);
        Log.d("###", "addBookmark: adding " + mSymposiumId + " into list.");
        mTinyDB.putListInt(Config.TINYDB_SYMPO, mBookmarkedSymposiumIds);
        isBookmarked = true;
    }

    private void removeBookmark() {
        Log.d("###", "removeBookmark: Removing bookmark.");
        mWorkshopBookmark.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_big_inactive));
        Log.d("###", "addBookmark: removing" + mSymposiumId + " from list.");
        for (int i = 0; i < mBookmarkedSymposiumIds.size(); i++)
            if (mBookmarkedSymposiumIds.get(i) == mSymposiumId)
                mBookmarkedSymposiumIds.remove(i);
        mTinyDB.putListInt(Config.TINYDB_SYMPO, mBookmarkedSymposiumIds);
        isBookmarked = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symposium_hack_detail);
        ButterKnife.bind(this);

        setupToolbar();
        loadBannerImage();
        mSymposiumId = getIntent().getIntExtra(EVENT_DETAIL_INDEX, 0);
        setupTinyDB();

        updateView();
    }

    private void setupTinyDB() {
        mTinyDB = new TinyDB(this.getApplicationContext());
        mBookmarkedSymposiumIds = mTinyDB.getListInt(Config.TINYDB_SYMPO);
        isBookmarked = mBookmarkedSymposiumIds.contains(mSymposiumId);
        if (isBookmarked)
            mWorkshopBookmark.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_big_active));
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder)
                .into(mBannerImageView);
    }

    private void updateView() {

        int contentResource = 0;
        int titleResource = 0;

        int[] titleDrawables = {
                R.drawable.plenarylectures_title,
                R.drawable.arryht_upd_title,
                R.drawable.specialpopulation_title,
                R.drawable.suddendeath_title,
                R.drawable.breakfast1_title,
                R.drawable.breakfast2_title,
                R.drawable.plenarylectures_title,
                R.drawable.afib_tittle,
                R.drawable.supraventicular_title,
                R.drawable.lunchanticoagulant_title,
                R.drawable.lunchpacemaker_title,
                R.drawable.syncope_title,
                R.drawable.tachy_title,
                R.drawable.deivcetherapy_title,
                R.drawable.heartfailure_title,
                R.drawable.medicalaf_title,
                R.drawable.unknown_tracing_ecg_title
        };

        int[] contentDrawables = {
                R.drawable.plenarylectures_content,
                R.drawable.arryht_upd_content,
                R.drawable.specialpopulation_content,
                R.drawable.suddendeath_content,
                R.drawable.breakfast1_content,
                R.drawable.breakfast2_content,
                R.drawable.plenarylectures_content,
                R.drawable.afib_content,
                R.drawable.supraventicular_content,
                R.drawable.lunchanticoagulant_content,
                R.drawable.lunchpacemaker_content,
                R.drawable.syncope_content,
                R.drawable.tachy_content,
                R.drawable.devicetherapy_content,
                R.drawable.heartfailure_content,
                0,
                0
        };

        switch (mSymposiumId) {
            case 1:
                contentResource = contentDrawables[0];
                titleResource = titleDrawables[0];
                break;
            case 9:
                contentResource = contentDrawables[1];
                titleResource = titleDrawables[1];
                break;
            case 10:
                contentResource = contentDrawables[2];
                titleResource = titleDrawables[2];
                break;
            case 11:
                contentResource = contentDrawables[3];
                titleResource = titleDrawables[3];
                break;
            case 12:
                contentResource = contentDrawables[4];
                titleResource = titleDrawables[4];
                break;
            case 13:
                contentResource = contentDrawables[5];
                titleResource = titleDrawables[5];
                break;
            case 15:
                contentResource = contentDrawables[6];
                titleResource = titleDrawables[6];
                break;
            case 16:
                contentResource = contentDrawables[7];
                titleResource = titleDrawables[7];
                break;
            case 17:
                contentResource = contentDrawables[8];
                titleResource = titleDrawables[8];
                break;
            case 18:
                contentResource = contentDrawables[9];
                titleResource = titleDrawables[9];
                break;
            case 19:
                contentResource = contentDrawables[10];
                titleResource = titleDrawables[10];
                break;
            case 20:
                contentResource = contentDrawables[11];
                titleResource = titleDrawables[11];
                break;
            case 21:
                contentResource = contentDrawables[12];
                titleResource = titleDrawables[12];
                break;
            case 22:
                contentResource = contentDrawables[13];
                titleResource = titleDrawables[13];
                break;
            case 23:
                contentResource = contentDrawables[14];
                titleResource = titleDrawables[14];
                break;
            case 24:
                contentResource = contentDrawables[15];
                titleResource = titleDrawables[15];
                break;
            case 25:
                contentResource = contentDrawables[16];
                titleResource = titleDrawables[16];
                break;
        }

        Glide.with(this).load(contentResource).into(mWorkshopContent);
        Glide.with(this).load(titleResource).into(mTitleImage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
