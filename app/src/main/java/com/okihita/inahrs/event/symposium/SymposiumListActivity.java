package com.okihita.inahrs.event.symposium;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;
import com.okihita.inahrs.util.Config;
import com.okihita.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SymposiumListActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.workshopList_RV_event)
    RecyclerView mProgramRecyclerView;
    List<Symposium> mSymposia = new ArrayList<>();
    RecyclerView.LayoutManager mLayoutManager;
    SymposiumAdapter mSymposiumAdapter;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symposium_list);
        ButterKnife.bind(this);

        setupToolbar();
        loadBannerImage();
        fetchSymposiaList();
        setupRecyclerView();
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder).
                into(mBannerImageView);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void fetchSymposiaList() {

        // Workaround untuk re-indexing spesialisasi
        String fetchURL = Config.SYMPOSIUM_GET_LIST;
        mSymposia = new ArrayList<>();

        JsonArrayRequest hotelListRequest = new JsonArrayRequest(
                fetchURL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject object = response.getJSONObject(i);
                                mSymposia.add(new Symposium(
                                        object.getInt("id"),
                                        object.getString("name"),
                                        Config.ENDPOINT + object.getString("image"),
                                        object.getString("description"),
                                        object.getString("date"),
                                        object.getString("start_time"),
                                        object.getString("end_time"),
                                        object.getString("venue")
                                ));
                                Log.d("###", "onResponse: " + object.getString("date"));
                            }

                            mSymposiumAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            Log.e("###", "onResponse: " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(hotelListRequest);
    }

    private void setupRecyclerView() {
        mLayoutManager = new LinearLayoutManager(this);
        mProgramRecyclerView.setLayoutManager(mLayoutManager);
        mSymposiumAdapter = new SymposiumAdapter(mSymposia);
        mProgramRecyclerView.setAdapter(mSymposiumAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}
