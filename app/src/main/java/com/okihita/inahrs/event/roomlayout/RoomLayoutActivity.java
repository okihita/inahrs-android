package com.okihita.inahrs.event.roomlayout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.senab.photoview.PhotoViewAttacher;

public class RoomLayoutActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.tabs)
    TabLayout mTabLayout;
    @BindView(R.id.pager)
    ViewPager mViewPager;
    PhotoViewAttacher mAttacher;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_layout);
        ButterKnife.bind(this);

        setupToolbar();
        loadBannerImage();

        setupTabs();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void setupTabs() {
        mViewPager.setAdapter(new RoomLayoutPagerAdapter(getSupportFragmentManager()));
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder).
                into(mBannerImageView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}
