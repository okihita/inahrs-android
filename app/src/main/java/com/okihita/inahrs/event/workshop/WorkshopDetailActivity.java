package com.okihita.inahrs.event.workshop;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;
import com.okihita.inahrs.util.Config;
import com.okihita.inahrs.util.TinyDB;
import com.okihita.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WorkshopDetailActivity extends AppCompatActivity {

    public static final String EVENT_DETAIL_INDEX = "com.example.okihita.inahrs.event_detail";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.workshopDetail_TV_name)
    TextView mName;
    @BindView(R.id.workshopDetail_IV_picture)
    ImageView mPicture;
    @BindView(R.id.workshopDetail_TV_description)
    TextView mDescription;
    @BindView(R.id.workshopDetail_TV_date)
    TextView mDate;
    @BindView(R.id.workshopDetail_TV_startEndTime)
    TextView mStartEndTime;
    @BindView(R.id.workshopDetail_TV_venue)
    TextView mVenue;

    @BindView(R.id.eventDetail_LL_bookmark)
    LinearLayout mBookmarkLL;
    @BindView(R.id.eventDetail_IV_bookmarkIcon)
    ImageView mBookmarkIcon;
    TinyDB mTinyDB;
    ArrayList<Integer> mBookmarkedWorkshopIds = new ArrayList<>();
    private int mWorkshopId;
    private boolean isBookmarked = false;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @OnClick(R.id.workshopDetail_FAB_question)
    void gotoAskQuestion() {
        Intent intent = new Intent(this, AskWorkshopQuestionActivity.class);
        intent.putExtra(AskWorkshopQuestionActivity.WORKSHOP_ID, mWorkshopId);
        startActivity(intent);
    }

    @OnClick(R.id.eventDetail_LL_bookmark)
    void onBookmarkClicked() {
        if (!isBookmarked) addBookmark();
        else removeBookmark();
    }

    private void addBookmark() {
        mBookmarkIcon.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_active));
        mBookmarkedWorkshopIds.add(mWorkshopId);
        mTinyDB.putListInt(Config.TINYDB_WORKSHOP, mBookmarkedWorkshopIds);
        isBookmarked = true;
    }

    private void removeBookmark() {
        mBookmarkIcon.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_inactive));
        for (int i = 0; i < mBookmarkedWorkshopIds.size(); i++)
            if (mBookmarkedWorkshopIds.get(i) == mWorkshopId)
                mBookmarkedWorkshopIds.remove(i);
        mTinyDB.putListInt(Config.TINYDB_WORKSHOP, mBookmarkedWorkshopIds);
        isBookmarked = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workshop_detail);
        ButterKnife.bind(this);

        setupToolbar();
        loadBannerImage();
        mWorkshopId = getIntent().getIntExtra(EVENT_DETAIL_INDEX, 0);
        fetchWorkshopDetail();

        updateAskButton();
        setupTinyDB();
    }

    private void setupTinyDB() {
        mTinyDB = new TinyDB(this.getApplicationContext());
        mBookmarkedWorkshopIds = mTinyDB.getListInt(Config.TINYDB_WORKSHOP);

        isBookmarked = mBookmarkedWorkshopIds.contains(mWorkshopId);
        if (isBookmarked)
            mBookmarkIcon.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_active));
    }

    private void updateAskButton() {
        String username = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext())
                .getString(getString(R.string.pref_fullname), null);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder)
                .into(mBannerImageView);
    }

    private void fetchWorkshopDetail() {
        Log.d("###", "fetchWorkshopDetail: ");
        JsonArrayRequest eventDetailRequest = new JsonArrayRequest(
                Config.WORKSHOP_ENDPOINT + String.valueOf(mWorkshopId),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("###", "onResponse: " + response.toString());
                        JSONObject object;
                        try {
                            object = response.getJSONObject(0);
                            updateView(object);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("###", "onErrorResponse: " + error.getMessage());
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(eventDetailRequest);
    }

    private void updateView(JSONObject object) throws JSONException {
        mName.setText(object.getString("workshop_name"));
        mDescription.setText(Html.fromHtml(object.getString("workshop_description")));

        Glide.with(this).load(Config.ENDPOINT + object.getString("workshop_image"))
                .into(mPicture);

        mDate.setText(object.getString("workshop_date"));
        mStartEndTime.setText(object.getString("workshop_start_time") + "-" + object.getString("workshop_end_time"));
        mVenue.setText(object.getString("workshop_venue"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}