package com.okihita.inahrs.event.symposium;


import com.okihita.inahrs.util.Config;

import org.json.JSONException;
import org.json.JSONObject;

public class Symposium {

    private int mId;
    private String mName;
    private String mImageUrl;
    private String mDescription;
    private String mDate;
    private String mStartTime;
    private String mEndTime;
    private String mPlace;

    public Symposium(int id, String name, String imageUrl, String description, String date, String startTime, String endTime, String place) {
        mId = id;
        mName = name;
        mImageUrl = imageUrl;
        mDescription = description;
        mDate = date;
        mStartTime = startTime;
        mEndTime = endTime;
        mPlace = place;
    }

    public Symposium(JSONObject object) throws JSONException {
        if (!object.isNull("id")) mId = object.getInt("id");
        if (!object.isNull("name")) mName = object.getString("name");
        if (!object.isNull("image")) mImageUrl = Config.ENDPOINT + object.getString("image");
        if (!object.isNull("description")) mDescription = object.getString("description");
        if (!object.isNull("date")) mDate = object.getString("date");
        if (!object.isNull("start_time")) mStartTime = object.getString("start_time");
        if (!object.isNull("end_time")) mEndTime = object.getString("end_time");
        if (!object.isNull("venue")) mPlace = object.getString("venue");
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getDate() {
        return mDate;
    }

    public String getStartTime() {
        return mStartTime;
    }

    public String getEndTime() {
        return mEndTime;
    }

    public String getPlace() {
        return mPlace;
    }
}
