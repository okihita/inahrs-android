package com.okihita.inahrs.event.symposium;

import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;
import com.okihita.inahrs.util.Config;
import com.okihita.inahrs.util.VolleySingleton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AskSympoQuestionActivity extends AppCompatActivity {

    public static final String SYMPOSIUM_ID = "com.okihita.inahrs.symposium_id";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.askWorkshopQuestion_ET_question)
    EditText mQuestionEditText;
    @BindView(R.id.askWorkshopQuestion_Button_postQuestion)
    Button mPostQuestionButton;
    int mSymposiumId;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @OnClick(R.id.askWorkshopQuestion_Button_postQuestion)
    void postQuestion() {
        String username = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext())
                .getString(getString(R.string.pref_fullname), null);
        String questionContent = mQuestionEditText.getText().toString();

        String uri = Uri.parse(Config.Q_SYMPOSIUM_ENDPOINT + mSymposiumId)
                .buildUpon()
                .appendQueryParameter("asker", username)
                .appendQueryParameter("content", questionContent)
                .build().toString();

        StringRequest postQuestionRequest = new StringRequest(
                uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(AskSympoQuestionActivity.this, "Question sent!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AskSympoQuestionActivity.this, "Failed to send. Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(postQuestionRequest);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_workshop_question);
        ButterKnife.bind(this);

        mSymposiumId = getIntent().getIntExtra(SYMPOSIUM_ID, 0);

        setupToolbar();
        loadBannerImage();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder)
                .into(mBannerImageView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
