package com.okihita.inahrs.event.workshop;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WorkshopAdapter extends RecyclerView.Adapter<WorkshopAdapter.ViewHolder> {

    List<Workshop> mWorkshops;

    public WorkshopAdapter(List<Workshop> workshops) {
        mWorkshops = workshops;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_event_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Workshop workshop = mWorkshops.get(position);

        Glide.with(holder.mImageView.getContext()).load(workshop.getImageUrl())
                .centerCrop()
                .into(holder.mImageView);

        holder.mNameTextView.setText(workshop.getName());

        String dateText = workshop.getDate() + ", " + workshop.getStartTime() + " - " + workshop.getEndTime();
        holder.mDateTimeTextView.setText(dateText);
        holder.mPlaceTextView.setText(workshop.getPlace());
    }

    @Override
    public int getItemCount() {
        return mWorkshops.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.eventItem_IV_image)
        ImageView mImageView;
        @BindView(R.id.eventItem_TV_name)
        TextView mNameTextView;
        @BindView(R.id.eventItem_TV_dateTime)
        TextView mDateTimeTextView;
        @BindView(R.id.eventItem_TV_place)
        TextView mPlaceTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewEventDetail(view);
                }

                private void viewEventDetail(View view) {
                    Context c = view.getContext();
                    Intent intent = new Intent(c, WorkshopDetailHackActivity.class);
                    intent.putExtra(WorkshopDetailActivity.EVENT_DETAIL_INDEX, mWorkshops.get(getLayoutPosition()).getId());
                    c.startActivity(intent);
                }
            });
        }
    }
}
