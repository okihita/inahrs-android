package com.okihita.inahrs.event.symposium;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class SympoTabbedPagerAdapter extends FragmentPagerAdapter {

    final private int PAGE_COUNT = 2;
    private String[] mPageTitles = {"Friday", "Saturday"};

    public SympoTabbedPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FridayFragment.newInstance();
            case 1:
                return SaturdayFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitles[position];
    }
}
