package com.okihita.inahrs.event.symposium;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SymposiumAdapter extends RecyclerView.Adapter<SymposiumAdapter.ViewHolder> {

    List<Symposium> mSymposia;

    public SymposiumAdapter(List<Symposium> symposia) {
        mSymposia = symposia;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_sympo_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Symposium symposium = mSymposia.get(position);
        Glide
                .with(holder.mImageView.getContext())
                .load(R.drawable.sympo_calendar)
                .fitCenter()
                .into(holder.mImageView)
        ;

        holder.mNameTextView.setText(symposium.getName());
        String dateText = symposium.getDate() + ", " + symposium.getStartTime() + " - " + symposium.getEndTime();
        holder.mDateTimeTextView.setText(dateText);
        holder.mPlaceTextView.setText(symposium.getPlace());
    }

    @Override
    public int getItemCount() {
        return mSymposia.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.eventItem_IV_image)
        ImageView mImageView;
        @BindView(R.id.eventItem_TV_name)
        TextView mNameTextView;
        @BindView(R.id.eventItem_TV_dateTime)
        TextView mDateTimeTextView;
        @BindView(R.id.eventItem_TV_place)
        TextView mPlaceTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewEventDetail(view);
                }

                private void viewEventDetail(View view) {
                    Context c = view.getContext();
                    Intent intent = new Intent(c, SymposiumDetailHacktivity.class);
                    intent.putExtra(SymposiumDetailHacktivity.EVENT_DETAIL_INDEX, mSymposia.get(getLayoutPosition()).getId());
                    c.startActivity(intent);
                }
            });
        }
    }
}
