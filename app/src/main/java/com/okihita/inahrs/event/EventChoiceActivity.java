package com.okihita.inahrs.event;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;
import com.okihita.inahrs.event.roomlayout.RoomLayoutActivity;
import com.okihita.inahrs.event.symposium.SympoTabbedActivity;
import com.okihita.inahrs.event.workshop.WorkshopListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EventChoiceActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.event_ImageView_symposium)
    ImageView mSymposiumImageView;
    @BindView(R.id.event_ImageView_workshop)
    ImageView mWorkshopImageView;
    @BindView(R.id.event_CardView_roomLayout)
    CardView mLayoutCardView;
    @BindView(R.id.event_IV_roomLayout)
    ImageView mRoomIV;
    String mPopupMessage = null;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @OnClick(R.id.event_CardView_symposium)
    void gotoSymSpecSelection() {
        Intent intent = new Intent(this, SympoTabbedActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.event_CardView_workshop)
    void gotoWorkshopList() {
        Intent intent = new Intent(this, WorkshopListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.event_CardView_roomLayout)
    void gotoRoomLayout() {
        Intent intent = new Intent(this, RoomLayoutActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        ButterKnife.bind(this);

        Log.d("###", "onCreate: Eventchoiceactivty created");

        mPopupMessage = getIntent().getStringExtra("popup_message");
        if (mPopupMessage == null) {
        } else {
            showPopup(mPopupMessage);
        }

        setupToolbar();
        loadCardImages();
    }

    private void showPopup(String mPopupMessage) {
        AlertDialog.Builder notificationPopupBuilder = new AlertDialog.Builder(this);
        notificationPopupBuilder.setMessage(mPopupMessage);
        notificationPopupBuilder.setCancelable(true);

        notificationPopupBuilder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = notificationPopupBuilder.create();
        dialog.show();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void loadCardImages() {
        Glide.with(this)
                .load("http://www.icao.int/Meetings/PBN-Symposium/Pictures/pic%202%20SYMP.jpg")
                .into(mSymposiumImageView);
        Glide.with(this)
                .load("http://www.bbraun.co.id/images/images_galleries/DSC_2680.JPG")
                .into(mWorkshopImageView);
        Glide.with(this)
                .load("https://s-media-cache-ak0.pinimg.com/564x/c4/f7/29/c4f729f30cc254ce694ff07123e702bf.jpg")
                .into(mRoomIV);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}
