package com.okihita.inahrs.event.workshop;


public class Workshop {

    private int mId;
    private String mName;
    private String mImageUrl;
    private String mDescription;
    private String mDate;
    private String mStartTime;
    private String mEndTime;
    private String mPlace;

    public Workshop(int id, String name, String imageUrl, String description, String date, String startTime, String endTime, String place) {
        mId = id;
        mName = name;
        mImageUrl = imageUrl;
        mDescription = description;
        mDate = date;
        mStartTime = startTime;
        mEndTime = endTime;
        mPlace = place;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getDate() {
        return mDate;
    }

    public String getStartTime() {
        return mStartTime;
    }

    public String getEndTime() {
        return mEndTime;
    }

    public String getPlace() {
        return mPlace;
    }
}
