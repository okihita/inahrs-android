package com.okihita.inahrs.event.roomlayout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.senab.photoview.PhotoViewAttacher;

public class GroundFloorFragment extends Fragment {

    @BindView(R.id.roomLayout_IV_floorPlan)
    ImageView mFloorPlan;

    PhotoViewAttacher mAttacher;

    public static GroundFloorFragment newInstance() {
        Bundle args = new Bundle();

        GroundFloorFragment fragment = new GroundFloorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_floor, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        prepareFloorPlan();
    }

    private void prepareFloorPlan() {
        Glide.with(this).load(R.drawable.ground).into(mFloorPlan);

        // THE MAGIC HAPPENS HERE
        mAttacher = new PhotoViewAttacher(mFloorPlan);
    }
}
