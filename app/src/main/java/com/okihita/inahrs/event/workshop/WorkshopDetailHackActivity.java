package com.okihita.inahrs.event.workshop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;
import com.okihita.inahrs.util.Config;
import com.okihita.inahrs.util.TinyDB;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WorkshopDetailHackActivity extends AppCompatActivity {

    public static final String EVENT_DETAIL_INDEX = "com.example.okihita.inahrs.event_detail";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;

    @BindView(R.id.titleImage)
    ImageView mTitleImage;
    @BindView(R.id.workshopBookmark)
    ImageView mWorkshopBookmark;
    @BindView(R.id.workshopContent)
    ImageView mWorkshopContent;

    TinyDB mTinyDB;
    ArrayList<Integer> mBookmarkedWorkshopIds = new ArrayList<>();
    private int mWorkshopId;
    private boolean isBookmarked = false;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @OnClick(R.id.askQuestionButton)
    void gotoAskQuestion() {
        Intent intent = new Intent(this, AskWorkshopQuestionActivity.class);
        intent.putExtra(AskWorkshopQuestionActivity.WORKSHOP_ID, mWorkshopId);
        startActivity(intent);
    }

    @OnClick(R.id.workshopBookmark)
    void onBookmarkClicked() {
        if (!isBookmarked) addBookmark();
        else removeBookmark();
    }

    private void addBookmark() {
        mWorkshopBookmark.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_big_active));
        mBookmarkedWorkshopIds.add(mWorkshopId);
        mTinyDB.putListInt(Config.TINYDB_WORKSHOP, mBookmarkedWorkshopIds);
        isBookmarked = true;
    }

    private void removeBookmark() {
        mWorkshopBookmark.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_big_inactive));
        for (int i = 0; i < mBookmarkedWorkshopIds.size(); i++)
            if (mBookmarkedWorkshopIds.get(i) == mWorkshopId)
                mBookmarkedWorkshopIds.remove(i);
        mTinyDB.putListInt(Config.TINYDB_WORKSHOP, mBookmarkedWorkshopIds);
        isBookmarked = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symposium_hack_detail);
        ButterKnife.bind(this);

        setupToolbar();
        loadBannerImage();
        mWorkshopId = getIntent().getIntExtra(EVENT_DETAIL_INDEX, 0);
        updateView();

        setupTinyDB();
    }

    private void setupTinyDB() {
        mTinyDB = new TinyDB(this.getApplicationContext());
        mBookmarkedWorkshopIds = mTinyDB.getListInt(Config.TINYDB_WORKSHOP);

        isBookmarked = mBookmarkedWorkshopIds.contains(mWorkshopId);
        if (isBookmarked)
            mWorkshopBookmark.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_big_active));
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder)
                .into(mBannerImageView);
    }

    private void updateView() {

        int contentResource = 0;
        int titleResource = 0;

        int[] contentDrawables = {
                R.drawable.ecg_content,
                R.drawable.arrythmia_content,
                R.drawable.pediatric_content,
                R.drawable.tachy_content,
                R.drawable.noac_content,
                R.drawable.internvention_content,
                R.drawable.holter_content,
                R.drawable.whatsnew_content,
                R.drawable.i2cp_content
        };

        int[] titleDrawables = {
                R.drawable.ecg_title,
                R.drawable.arrythmia_ittle,
                R.drawable.pediatric_title,
                R.drawable.tachy_title,
                R.drawable.noac_title,
                R.drawable.intervention_title,
                R.drawable.holter_title,
                R.drawable.whatsnew_title,
                R.drawable.i2cp_title
        };

        switch (mWorkshopId) {
            case 1:
                contentResource = contentDrawables[0];
                titleResource = titleDrawables[0];
                break;
            case 2:
                contentResource = contentDrawables[1];
                titleResource = titleDrawables[1];
                break;
            case 3:
                contentResource = contentDrawables[2];
                titleResource = titleDrawables[2];
                break;
            case 4:
                contentResource = contentDrawables[3];
                titleResource = titleDrawables[3];
                break;
            case 5:
                contentResource = contentDrawables[4];
                titleResource = titleDrawables[4];
                break;
            case 6:
                contentResource = contentDrawables[5];
                titleResource = titleDrawables[5];
                break;
            case 7:
                contentResource = contentDrawables[6];
                titleResource = titleDrawables[6];
                break;
            case 9:
                contentResource = contentDrawables[7];
                titleResource = titleDrawables[7];
                break;
            case 10:
                contentResource = contentDrawables[8];
                titleResource = titleDrawables[8];
                break;
        }

        Glide.with(this).load(contentResource).into(mWorkshopContent);
        Glide.with(this).load(titleResource).into(mTitleImage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}