package com.okihita.inahrs.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.okihita.inahrs.R;
import com.okihita.inahrs.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_TIME_OUT = 3000;

    @BindView(R.id.splash_IV_background)
    ImageView mBackgroundIV;
    @BindView(R.id.splash_IV_banner)
    ImageView mBannerIV;
    @BindView(R.id.splash_IV_title)
    ImageView mTitleImageView;
    @BindView(R.id.splash_IV_footer)
    ImageView mFooterImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        loadImages();
        countdownToMainScreen();
    }

    private void loadImages() {
        Glide.with(this) // background
                .load(R.drawable.hexasplash)
                .centerCrop().crossFade()
                .into(mBackgroundIV);

        Glide.with(this) // banner
                .load(R.drawable.splash_banner)
                .centerCrop().crossFade()
                .into(mBannerIV);

        Glide.with(this) // title
                .load(R.drawable.splash_title)
                .crossFade()
                .into(mTitleImageView);

        Glide.with(this) // footer
                .load(R.drawable.splash_footer)
                .centerCrop().crossFade()
                .into(mFooterImageView);
    }

    private void countdownToMainScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
