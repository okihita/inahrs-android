package com.okihita.inahrs.speaker;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.okihita.inahrs.R;
import com.okihita.inahrs.util.Config;
import com.okihita.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SpeakerListActivity extends AppCompatActivity {

    private static final String TAG = Config.TAG;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.speakerList_RV_speakers)
    RecyclerView mSpeakerRecyclerView;
    ProgressDialog mProgressDialog;
    private List<SpeakerListItem> mSpeakerListItems;
    private SpeakerListItemAdapter mSpeakerListItemAdapter;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speaker_list);
        ButterKnife.bind(this);

        setupProgressDialog();
        setupToolbar();
        populateSpeakerListItem();
        setupRecyclerView();
    }

    private void setupProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void populateSpeakerListItem() {
        mProgressDialog.show();
        mSpeakerListItems = new ArrayList<>();
        JsonArrayRequest hotelListRequest = new JsonArrayRequest(
                Config.SPEAKER_GET_LIST,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject object = response.getJSONObject(i);
                                mSpeakerListItems.add(new SpeakerListItem(
                                        object.getInt("id"),
                                        Config.ENDPOINT + object.getString("image"),
                                        object.getString("name"),
                                        object.getString("nationality"),
                                        object.getString("title")
                                ));
                                Log.d(TAG, "onResponse: Added: " + object.getString("name"));
                            }
                            mProgressDialog.dismiss();
                            mSpeakerListItemAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            Log.d(TAG, "onResponse: Error " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(hotelListRequest);
    }

    private void setupRecyclerView() {
        mSpeakerRecyclerView.setHasFixedSize(true);
        mSpeakerListItemAdapter = new SpeakerListItemAdapter(mSpeakerListItems);
        mSpeakerRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mSpeakerRecyclerView.setAdapter(mSpeakerListItemAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

}
