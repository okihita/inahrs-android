package com.okihita.inahrs.speaker;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.okihita.inahrs.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SpeakerListItemAdapter extends RecyclerView.Adapter<SpeakerListItemAdapter.ViewHolder> {

    private List<SpeakerListItem> mSpeakerListItems;

    public SpeakerListItemAdapter(List<SpeakerListItem> speakerListItems) {
        mSpeakerListItems = speakerListItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_speaker_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        SpeakerListItem speakerListItem = mSpeakerListItems.get(position);

        Glide.with(holder.mImageView.getContext())
                .load(speakerListItem.getImageURL())
                .asBitmap()
                .centerCrop()
                .into(new BitmapImageViewTarget(holder.mImageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(holder.mImageView.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        holder.mImageView.setImageDrawable(circularBitmapDrawable);
                    }
                });

        holder.mNameTextView.setText(speakerListItem.getName());
        holder.mTitleTextView.setText(speakerListItem.getTitle());
        holder.mNationalityTextView.setText(speakerListItem.getNationality());
    }

    @Override
    public int getItemCount() {
        return mSpeakerListItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.speakerItem_IV_picture)
        ImageView mImageView;
        @BindView(R.id.speakerItem_TV_name)
        TextView mNameTextView;
        @BindView(R.id.speakerItem_TV_title)
        TextView mTitleTextView;
        @BindView(R.id.speakerItem_TV_nationality)
        TextView mNationalityTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int speakerId = mSpeakerListItems.get(getLayoutPosition()).getId();
                    Intent intent = new Intent(v.getContext(), SpeakerDetailActivity.class);
                    intent.putExtra(SpeakerDetailActivity.SPEAKER_DETAIL_ID_INTENT, speakerId);
                    v.getContext().startActivity(intent);
                }
            });
        }
    }
}
