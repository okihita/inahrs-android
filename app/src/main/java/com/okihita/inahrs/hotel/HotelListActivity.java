package com.okihita.inahrs.hotel;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.okihita.inahrs.R;
import com.okihita.inahrs.util.Config;
import com.okihita.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HotelListActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.hotelList_RV_speakers)
    RecyclerView mRecyclerView;
    List<HotelListItem> mHotelListItems;
    RecyclerView.LayoutManager mLayoutManager;
    HotelListItemAdapter mHotelListItemAdapter;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_list);
        ButterKnife.bind(this);

        setupToolbar();

        populateHotelItems();
        setupRecyclerView();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void populateHotelItems() {

        mHotelListItems = new ArrayList<>();

        JsonArrayRequest hotelListRequest = new JsonArrayRequest(
                Config.HOTEL_GET_LIST,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.d("###", "onResponse: " + response.toString());
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject object = response.getJSONObject(i);
                                mHotelListItems.add(new HotelListItem(
                                        object.getInt("id"),
                                        object.getString("name"),
                                        Config.ENDPOINT + object.getString("image"),
                                        object.getString("distance")
                                ));
                            }

                            mHotelListItemAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            Log.d("###", "onResponse: Error " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("###", "onErrorResponse: " + error.getMessage());
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(hotelListRequest);
    }

    private void setupRecyclerView() {
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mHotelListItemAdapter = new HotelListItemAdapter(mHotelListItems);
        mRecyclerView.setAdapter(mHotelListItemAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

}
